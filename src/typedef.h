#include <sys/types.h>

#if !defined(FALSE)
#define FALSE    0
#endif
#if !defined(TRUE)
#define TRUE     1
#endif
typedef int bool;
// Structure types.
typedef struct  affect_data      AFFECT_DATA;
typedef struct  area_data      AREA_DATA;
  typedef struct  ban_data      BAN_DATA;
typedef struct  buf_type      BUFFER;
typedef struct  char_data      CHAR_DATA;
typedef struct  descriptor_data    DESCRIPTOR_DATA;
typedef struct  exit_data      EXIT_DATA;
typedef struct  extra_descr_data  EXTRA_DESCR_DATA;
  typedef struct  help_data      HELP_DATA;
  typedef struct  help_area_data    HELP_AREA;
typedef struct  kill_data      KILL_DATA;
typedef struct  mem_data      MEM_DATA;
typedef struct  mob_index_data    MOB_INDEX_DATA;
  typedef struct  note_data      NOTE_DATA;
typedef struct  obj_data      OBJ_DATA;
typedef struct  obj_index_data    OBJ_INDEX_DATA;
typedef struct  pc_data        PC_DATA;
  typedef struct  gen_data      GEN_DATA;
typedef struct  reset_data      RESET_DATA;
typedef struct  room_index_data    ROOM_INDEX_DATA;
typedef struct  shop_data      SHOP_DATA;
typedef struct  time_info_data    TIME_INFO_DATA;
typedef struct  weather_data    WEATHER_DATA;
  typedef struct  prog_list      PROG_LIST;
  typedef struct  prog_code      PROG_CODE;
  typedef struct  color_data      COLOR_DATA;
  typedef struct  intercept_code    INTERCEPT_CODE;
  typedef struct  event_data      EVENT_DATA;  //SF

typedef union vo_t                  vo_t;

union vo_t {
        int                     vnum;
        ROOM_INDEX_DATA *       r;
        OBJ_INDEX_DATA *        o;
        MOB_INDEX_DATA *        m;
};

// Function types
typedef void    DO_FUN      (CHAR_DATA *ch, const char *argument);
typedef bool    SPEC_FUN    (CHAR_DATA *ch);
typedef void    SPELL_FUN   (int sn, int level, CHAR_DATA *ch, void *vo, int target);
typedef int     OPROG_FUN   (OBJ_DATA *obj, CHAR_DATA *ch, const void *arg);
typedef void    OBJ_FUN     (OBJ_DATA *obj, const char *argument);
typedef void    ROOM_FUN    (ROOM_INDEX_DATA *room, const char *argument);
  typedef  bool  EVENT_FUN    (EVENT_DATA *event);  //SF

#define args(a) a
#define DECLARE_DO_FUN(fun)     DO_FUN    fun
#define DECLARE_SPEC_FUN(fun)   SPEC_FUN  fun
#define DECLARE_SPELL_FUN(fun)  SPELL_FUN fun
#define DECLARE_OPROG_FUN(fun)  OPROG_FUN fun
#define DECLARE_OBJ_FUN(fun)    OBJ_FUN   fun
#define DECLARE_ROOM_FUN(fun)   ROOM_FUN  fun

#define DO_FUN(fun)     void    fun(CHAR_DATA *ch, const char *argument)
#define SPEC_FUN(fun)   bool    fun(CHAR_DATA *ch)
#define SPELL_FUN(fun)  void    fun(int sn, int level, CHAR_DATA *ch, void *vo, int target)
#define OPROG_FUN(fun)  int     fun(OBJ_DATA *obj, CHAR_DATA *ch, void *arg);
#define OBJ_FUN(fun)    void    fun(OBJ_DATA *obj, const char *argument);
#define ROOM_FUN(fun)   void    fun(ROOM_INDEX_DATA *room, const char *argument);
  #define EVENT_FUN(fun)  bool  fun(EVENT_DATA *event);  //SF
