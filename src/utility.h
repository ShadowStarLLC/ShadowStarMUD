/* Utility macros */
#define IS_VALID(data)        ((data) != NULL && (data)->valid)
#define VALIDATE(data)        ((data)->valid = TRUE)
#define INVALIDATE(data)      ((data)->valid = FALSE)
#define UMIN(a, b)            ((a) < (b) ? (a) : (b))
#define UMAX(a, b)            ((a) > (b) ? (a) : (b))
#define URANGE(a, b, c)       ((b) < (a) ? (a) : ((b) > (c) ? (c) : (b)))
#define LOWER(c)              ((c) >= 'A' && (c) <= 'Z' ? (c)+'a'-'A' : (c))
#define UPPER(c)              ((c) >= 'a' && (c) <= 'z' ? (c)+'A'-'a' : (c))
#define IS_SET(flag, bit)     ((flag) & (bit))
#define SET_BIT(var, bit)     ((var) |= (bit))
#define REMOVE_BIT(var, bit)  ((var) &= ~(bit))
#define IS_NULLSTR(str)       ((str) == NULL || (str)[0] == '\0')
#define ENTRE(min,num,max)    (((min) < (num)) && ((num) < (max)) )
#define CHECK_POS(a, b, c)    {(a) = (b); if ( (a) < 0 ){ bug( "CHECK_POS : " c " == %d < 0", a );}}

/* Infinate flag macros */
#define STR_ZERO_STR(var,size)  { int i; for(i=0;i<(size);i++){(var)[i]=0;} }
#define STR_SAME_STR(a,b,size)  (memcmp((a),(b),(size))==0)
#define STR_AND_STR(a,b,size)  { int i;for(i=0;i<(size);i++){(a)[i]&=(b)[i];} }
#define STR_OR_STR(a,b,size)  { int i;for(i=0;i<(size);i++){(a)[i]|=(b)[i];} }
#define STR_XOR_STR(a,b,size)  { int i;for(i=0;i<(size);i++){(a)[i]^=(b)[i];} }
#define STR_NOT_STR(a,size)    { int i;for(i=0;i<(size);i++){(a)[i]=~(a)[i];} }
#define STR_REMOVE_STR(a,b,size){ int i;for(i=0;i<(size);i++){(a)[i]&=~((b)[i]);} }
#define STR_IS_SET(var,bit)    ((((FLAG *)(var))[((bit)/FLAG_BITSIZE)]) &   ((1<<((bit)%FLAG_BITSIZE))))
#define STR_SET_BIT(var,bit)  ((((FLAG *)(var))[((bit)/FLAG_BITSIZE)]) |=  ((1<<((bit)%FLAG_BITSIZE))))
#define STR_REMOVE_BIT(var,bit)  ((((FLAG *)(var))[((bit)/FLAG_BITSIZE)]) &= ~((1<<((bit)%FLAG_BITSIZE))))
#define STR_TOGGLE_BIT(var,bit)  ((((FLAG *)(var))[((bit)/FLAG_BITSIZE)]) ^=  ((1<<((bit)%FLAG_BITSIZE))))

/* Character macros */
#define IS_NPC(ch)        (IS_SET((ch)->act, ACT_IS_NPC))
#define IS_ADMIN(ch)        (get_trust(ch) >= LEVEL_ADMIN)
#define IS_IMMORTAL(ch)    (get_trust(ch) >= LEVEL_IMMORTAL)
#define IS_TRUSTED(ch,level)    (get_trust((ch)) >= (level))
#define IS_AFFECTED(ch, sn)  (STR_IS_SET((ch)->affected_by, (sn)))

#define GET_AGE(ch)        ((int) (17 + ((ch)->played \
  + current_time - (ch)->logon )/72000))

#define IS_GOOD(ch)        (ch->alignment >= 350)
#define IS_EVIL(ch)        (ch->alignment <= -350)
#define IS_NEUTRAL(ch)        (!IS_GOOD(ch) && !IS_EVIL(ch))

#define IS_AWAKE(ch)        (ch->position > POS_SLEEPING)
#define GET_AC(ch,type)        ((ch)->armor[type]  \
  + ( IS_AWAKE(ch)  \
  ? dex_app[get_curr_stat(ch,STAT_DEX)].defensive : 0 ))
#define GET_HITROLL(ch)  \
  ((ch)->hitroll+str_app[get_curr_stat(ch,STAT_STR)].tohit)
#define GET_DAMROLL(ch) \
  ((ch)->damroll+str_app[get_curr_stat(ch,STAT_STR)].todam)

#define IS_OUTSIDE(ch)        (!IS_SET(  \
  (ch)->in_room->room_flags,  \
  ROOM_INDOORS))

#define WAIT_STATE(ch, npulse)    ((ch)->wait = UMAX((ch)->wait, (npulse)))
#define DAZE_STATE(ch, npulse)  ((ch)->daze = UMAX((ch)->daze, (npulse)))
#define get_carry_weight(ch)    ((ch)->carry_weight + (ch)->silver/10 +  \
  (ch)->gold * 2 / 5)

#define act(format,ch,arg1,arg2,type)  \
  act_new((format),(ch),(arg1),(arg2),(type),POS_RESTING)

#define HAS_TRIGGER(ch,trig)    (IS_SET((ch)->pIndexData->mprog_flags,(trig)))
#define HAS_TRIGGER_OBJ(obj,trig) (IS_SET((obj)->pIndexData->oprog_flags,(trig)))
#define HAS_TRIGGER_ROOM(room,trig) (IS_SET((room)->rprog_flags,(trig)))
#define IS_SWITCHED( ch )       ( ch->desc && ch->desc->original )
#define IS_BUILDER(ch, Area)    ( !IS_NPC(ch) && !IS_SWITCHED( ch ) &&  \
  ( ch->pcdata->security >= Area->security  \
  || strstr( Area->builders, ch->name )  \
  || strstr( Area->builders, "All" ) ) )

/* Object macros */
#define CAN_WEAR(obj, part)    (IS_SET((obj)->wear_flags,  (part)))
#define IS_OBJ_STAT(obj, stat)    (IS_SET((obj)->extra_flags, (stat)))
#define IS_WEAPON_STAT(obj,stat)(IS_SET((obj)->value[4],(stat)))
#define WEIGHT_MULT(obj)    ((obj)->item_type == ITEM_CONTAINER ? \
  (obj)->value[4] : 100)

/* Description macros */
#define PERS(ch, looker)    ( can_see( looker, (ch) ) ?  \
  ( IS_NPC(ch) ? (ch)->short_descr  \
  : (ch)->name ) : "someone" )

/* Uncategorized macros */
#define CH(d) ((d)->original ? (d)->original : (d)->character)
