//#include <sys/types.h>
//#include <sys/utime.h>
////#include <sys/resource.h>
//#endif
//
//#include "utility.h"
//
///* Utility Commands */
//int number_bits (int width)
//{
//  return number_mm () & ((1 << width) - 1);
//}
//
///* Stick a little fuzz on a number */
//// 25% chance of +/- 1, 50% chance of doing nothing
//int number_fuzzy (int number)
//{
//  switch (number_bits (2))
//  {
//  case 0:
//    number -= 1;
//    break;
//  case 3:
//    number += 1;
//    break;
//  }
//
//  return UMAX (1, number);
//}
//
///* Generate a random number */
//int number_range (int from, int to)
//{
//  int power;
//  int number;
//
//  if (from == 0 && to == 0)
//    return 0;
//
//  if ((to = to - from + 1) <= 1)
//    return from;
//
//  for (power = 2; power < to; power <<= 1);
//
//  while ((number = number_mm () & (power - 1)) >= to);
//
//  return from + number;
//}
//
///* Generate a percentile roll */
//int number_percent (void)
//{
//  int percent;
//
//  while ((percent = number_mm () & (128 - 1)) > 99);
//
//  return 1 + percent;
//}
//
///* Generate a random door */
//int number_door (void)
//{
//  int door;
//
//  while ((door = number_mm () & (8 - 1)) > 5);
//
//  return door;
//}
//
///* Generate a random bit */
//// Binary shift: 1 << 3 -> 1000 - 0001 -> 8 - 1 = 7 -> [0, 7]
//// num_bit(5) = 0, 31 -> 100000 - 1 = 31
//// num_bit(4) = 0, 15 -> 10000  - 1 = 15
//// num_bit(3) = 0, 7  -> 1000   - 1 =  7
//void init_mm ()
//{
//#if defined (OLD_RAND)
//  int *piState;
//  int iState;
//
//  piState = &rgiState[2];
//
//  piState[-2] = 55 - 55;
//  piState[-1] = 55 - 24;
//
//  piState[0] = ((int) current_time) & ((1 << 30) - 1);
//  piState[1] = 1;
//  for (iState = 2; iState < 55; iState++)
//  {
//    piState[iState] = (piState[iState - 1] + piState[iState - 2]) & ((1 << 30) - 1);
//  }
//#else
//  srandom (time (NULL) ^ getpid ());
//#endif
//  return;
//}
//
//
//
//long number_mm (void)
//{
//#if defined (OLD_RAND)
//  int *piState;
//  int iState1;
//  int iState2;
//  int iRand;
//
//  piState = &rgiState[2];
//  iState1 = piState[-2];
//  iState2 = piState[-1];
//  iRand = (piState[iState1] + piState[iState2]) & ((1 << 30) - 1);
//  piState[iState1] = iRand;
//  if (++iState1 == 55)
//    iState1 = 0;
//  if (++iState2 == 55)
//    iState2 = 0;
//  piState[-2] = iState1;
//  piState[-1] = iState2;
//  return iRand >> 6;
//#else
//  return random () >> 6;
//#endif
//}
//
//
///* Roll some dice */
//int dice (int number, int size)
//{
//  int idice;
//  int sum;
//
//  switch (size)
//  {
//  case 0:
//    return 0;
//  case 1:
//    return number;
//  }
//
//  for (idice = 0, sum = 0; idice < number; idice++)
//    sum += number_range (1, size);
//
//  return sum;
//}
//
//
//
///* Simple linear interpolation */
//int interpolate (int level, int value_00, int value_32)
//{
//  return value_00 + level * (value_32 - value_00) / 32;
//
//  /*
//    .-       -.
//    | Y2 - Y1 |
//    | ------- | * ( X - X1 ) + Y1 = Y
//    | X2 - X1 |
//    '-       -'
//    return Y_1 + ( X - X_1 ) * ( ( Y_2 - Y_1 ) / ( X_2 - X_1 ) )
//  */
//}
//
//int interpolation (int Xvalue, int Xvalue_00, int Xvalue_32, int Yvalue_00, int Yvalue_32)
//{
//  return Yvalue_00 + (Xvalue - Xvalue_00) * ((Yvalue_32 - Yvalue_00) / (Xvalue_32 - Xvalue_00));
//}
//
//
///*
//* Removes the tildes from a string.
//* Used for player-entered strings that go into disk files.
//*/
//void smash_tilde (char *str)
//{
//  for (; *str != '\0'; str++)
//  {
//    if (*str == '~')
//      *str = '-';
//  }
//
//  return;
//}
///*
//* Removes dollar signs to keep snerts from crashing us.
//* Posted to ROM list by Kyndig. JR -- 10/15/00
//*/
//void smash_dollar( char *str )
//{
//  for( ; *str != '\0'; str++)
//  {
//    if( *str == '$' )
//      *str = 'S';
//  }
//  return;
//}
//
///* Compare strings, case insensitive -
//Return TRUE if different (compatibility with historical functions) */
//bool str_cmp (const char *astr, const char *bstr)
//{
//  if (astr == NULL)
//  {
//    bug ("Str_cmp: null astr.", 0);
//    return TRUE;
//  }
//
//  if (bstr == NULL)
//  {
//    bug ("Str_cmp: null bstr.", 0);
//    return TRUE;
//  }
//
//  for (; *astr || *bstr; astr++, bstr++)
//  {
//    if (LOWER (*astr) != LOWER (*bstr))
//      return TRUE;
//  }
//
//  return FALSE;
//}
//
///* Compare strings, case insensitive, for prefix matching -
//Return TRUE if astr not a prefix of bstr (compatibility with historical functions) */
//bool str_prefix (const char *astr, const char *bstr)
//{
//  if (astr == NULL)
//  {
//    bug ("Strn_cmp: null astr.", 0);
//    return TRUE;
//  }
//
//  if (bstr == NULL)
//  {
//    bug ("Strn_cmp: null bstr.", 0);
//    return TRUE;
//  }
//
//  for (; *astr; astr++, bstr++)
//  {
//    if (LOWER (*astr) != LOWER (*bstr))
//      return TRUE;
//  }
//
//  return FALSE;
//}
//
///* Compare strings, case insensitive, for match anywhere  -
//Returns TRUE is astr not part of bstr (compatibility with historical functions) */
//bool str_infix (const char *astr, const char *bstr)
//{
//  int sstr1;
//  int sstr2;
//  int ichar;
//  char c0;
//
//  if ((c0 = LOWER (astr[0])) == '\0')
//    return FALSE;
//
//  sstr1 = strlen (astr);
//  sstr2 = strlen (bstr);
//
//  for (ichar = 0; ichar <= sstr2 - sstr1; ichar++)
//  {
//    if (c0 == LOWER (bstr[ichar]) && !str_prefix (astr, bstr + ichar))
//      return FALSE;
//  }
//
//  return TRUE;
//}
//
//
//
///*
//* Compare strings, case insensitive, for suffix matching.
//* Return TRUE if astr not a suffix of bstr
//*   (compatibility with historical functions).
//*/
//bool str_suffix (const char *astr, const char *bstr)
//{
//  int sstr1;
//  int sstr2;
//
//  sstr1 = strlen (astr);
//  sstr2 = strlen (bstr);
//  if (sstr1 <= sstr2 && !str_cmp (astr, bstr + sstr2 - sstr1))
//    return FALSE;
//  else
//    return TRUE;
//}
//
//
//
///*
//* Returns an initial-capped string.
//*/
//char *capitalize (const char *str)
//{
//  static char strcap[MAX_STRING_LENGTH];
//  int i;
//
//  for (i = 0; str[i] != '\0'; i++)
//    strcap[i] = LOWER (str[i]);
//  strcap[i] = '\0';
//  strcap[0] = UPPER (strcap[0]);
//  return strcap;
//}
